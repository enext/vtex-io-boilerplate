import React from 'react';

const Hello = () => {
  return(
    <p>
      React builder ready to use! Just implement your code here!
    </p>
  )
};

export default Hello;